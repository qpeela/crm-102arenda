import React, { Component } from 'react';

import Modal from './modal';

export default class ButtonsGroups extends Component {
    constructor(props) {
        super(props);

        this.state = {
        }
    }

    render() {
        return (
            <div>
                <button type="button" className="btn btn-info btn-lg" data-toggle="modal" data-target="#modalAddObj">Добавить объект</button>
                <Modal />
            </div>
        );
    }
}