import React, { Component } from 'react';
import { Image } from 'cloudinary-react';
import {CloudinaryContext, Transformation} from 'cloudinary-react';

import ID from '/client/imports/utils/generatorId';
import Tasks from '../../../../api/tasks';
import DG from '2gis-maps';

import './modal.scss';

export default class Modal extends Component {
    constructor(props) {
        super(props);

        this.save = this.save.bind(this);
        this.uploadWidget = this.uploadWidget.bind(this);

        this.state = {
            images: [],
        }
    }

    uploadWidget(e) {
        e.preventDefault();
        let images = [];

        window.cloudinary.openUploadWidget({ cloud_name: 'arenda102', upload_preset: 'wxckmis7', tags:['xmas']},
            (error, result) => {
                images = result.map((item) => item.public_id);
                this.setState({ images });
            });
    }

    save() {
        const countRooms = document.getElementById('countRooms').value;
        const typeObject = document.getElementById('typeObject').value;
        const district = document.getElementById('district').value;
        const floorFrom = document.getElementById('floorFrom').value;
        const floorTo = document.getElementById('floorTo').value;
        const area = document.getElementById('area').value;
        const address = document.getElementById('address').value;
        const price = document.getElementById('price').value;
        const phone = document.getElementById('phone').value;
        const desc = document.getElementById('desc').value;

        Tasks.insert({
            id: ID(),
            createdAt: new Date(), // current time
            createdUser: Meteor.userId(),
            countRooms,
            desc,
            typeObject,
            district,
            floorFrom,
            floorTo,
            area,
            price,
            address,
            phone,
            images: this.state.images,
        });

        console.log('insert');
    }

    render() {
        const images = this.state.images;
        return (
            <div id="modalAddObj" className="modal fade " role="dialog">
                <div className="modal-dialog modal-lg">

                    <div className="modal-content">
                        <div className="modal-header">
                            <button type="button" className="close" data-dismiss="modal">&times;</button>
                            <h4 className="modal-title">Добавление объекта</h4>
                        </div>
                        <div className="modal-body">
                            <form className="form-inline">
                                <div className="form-group">
                                    <label htmlFor="typeObject">Объект</label>
                                    <select className="form-control" id="typeObject">
                                        <option value="flat">Квартира</option>
                                        <option value="room">Комната</option>
                                        <option value="house">Дом</option>
                                        <option value="maloSemeika">Малосемейка</option>
                                        <option value="cottedge">Коттедж</option>
                                    </select>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="countRooms">Количество комнат</label>
                                    <select className="form-control" id="countRooms">
                                        <option value="room">комната</option>
                                        <option value="1studio">1-студия</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                    </select>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="district">Район</label>
                                    <select className="form-control" id="district">
                                        <option value="center">Центр</option>
                                        <option value="prospekt">Проспект</option>
                                        <option value="sip">Сипайлово</option>
                                        <option value="zel">Зеленка</option>
                                        <option value="tcBash">ТЦ Башкирия</option>
                                        <option value="mvd">МВД</option>
                                        <option value="chern">Черниковка</option>
                                        <option value="dem">Дема</option>
                                        <option value="zaton">Затон</option>
                                        <option value="inors">Инорс</option>
                                        <option value="saksha">Шакша</option>
                                        <option value="nizh">Нижегородка</option>
                                    </select>
                                </div>
                                <br />
                                <div className="form-group">
                                    <label htmlFor="floorFrom">Этаж</label>
                                    <input type="number" className="form-control" id="floorFrom" />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="floorTo">Этаж до</label>
                                    <input type="number" className="form-control" id="floorTo" />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="area">Площадь</label>
                                    <input type="number" className="form-control" id="area" /> м<sup>2</sup>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="address">Адрес</label>
                                    <input type="text" className="form-control" id="address" />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="price">Цена</label>
                                    <input type="text" className="form-control" id="price" />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="phone">Телефон</label>
                                    <input type="text" className="form-control" id="phone" />
                                </div>
                                <br />
                                <div className="form-group row" style={{ width: '100%' }}>
                                    <div className="col-xs-12">
                                        <label htmlFor="desc">Описание</label>
                                        <textarea className="form-control" rows="10" id="desc"></textarea>
                                    </div>
                                </div>
                                <br />
                                <div>
                                    {
                                        images.map((item) => {
                                            return (
                                                <Image
                                                    key={Math.random() * 1000}
                                                    cloudName="arenda102"
                                                    publicId={item}
                                                    width="300"
                                                    crop="scale"
                                                />
                                            );
                                        })
                                    }
                                </div>
                                <br />
                                <button type="button" onClick={this.uploadWidget} className="btn btn-primary">Загрузить фото</button>
                                <div id="map"></div>
                            </form>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-success" onClick={this.save}>Сохранить</button>
                            <button type="button" className="btn btn-default" data-dismiss="modal">Отмена</button>
                        </div>
                    </div>

                </div>
            </div>
        );
    }
}
