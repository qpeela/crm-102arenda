import React, { Component } from 'react';
import { createContainer } from 'meteor/react-meteor-data';

import { typeObject, countRooms, district } from '/client/imports/utils/constants';

import Tasks from '../../../api/tasks';

import ButtonsGroup from './components/buttonsGroups.jsx';

class DbObjectsPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            fl: null,
        };
    }

    render() {
        console.log(this.props.users);

        return (
            <div>
                <ButtonsGroup />
                <table className="table">
                    <thead>
                        <tr>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.props.tasks.map((item) => {
                                return (
                                    <tr key={Math.random()}>
                                        <td>{typeObject[item.typeObject]}</td>
                                        <td>{countRooms[item.countRooms]}</td>
                                        <td>{district[item.district]}</td>
                                    </tr>
                                );
                            })
                        }
                    </tbody>
                </table>
            </div>
        );
    }
}

export default createContainer(() => {
    return {
        tasks: Tasks.find({}).fetch(),
        users: Meteor.users.find({}).fetch(),
    }
}, DbObjectsPage);