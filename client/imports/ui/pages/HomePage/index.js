import React, { Component } from 'react';
import { createContainer } from 'meteor/react-meteor-data';
import Tasks from "../../../api/tasks";

class HomePage extends Component {
    constructor(props) {
        super(props);

        console.log(this.props);
        this.state = {
            fl: null,
        };
    }

    render() {
        return (
            <div>
                Home
            </div>
        );
    }
}

export default createContainer(() => {
    return {
        tasks: Tasks.find({}).fetch(),
    }
}, HomePage);