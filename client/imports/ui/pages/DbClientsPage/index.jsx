import React, { Component } from 'react';
import { createContainer } from 'meteor/react-meteor-data';
import { Clients } from "../../../api/clients";

import ButtonsGroup from './components/buttonsGroups.jsx';

class DbClientsPage extends Component {
    constructor(props) {
        super(props);

        console.log(this.props);
        this.state = {
            fl: null,
        };
    }

    render() {
        return (
            <div>
                <ButtonsGroup />
                <table className="table">

                </table>
            </div>
        );
    }
}

export default createContainer(() => {
    return {
        clients: Clients.find({}).fetch(),
    }
}, DbClientsPage);