import React, { Component } from 'react';

import ID from '/client/imports/utils/generatorId';
import { Tasks } from '../../../../api/tasks';

import './modal.scss';

export default class Modal extends Component {
    constructor(props) {
        super(props);
        console.log(ID());

        this.save = this.save.bind(this);
    }

    save() {
    }

    render() {
        return (
            <div id="modal" className="modal fade " role="dialog">
                <div className="modal-dialog modal-lg">

                    <div className="modal-content">
                        <div className="modal-header">
                            <button type="button" className="close" data-dismiss="modal">&times;</button>
                            <h4 className="modal-title">Добавление объекта</h4>
                        </div>
                        <div className="modal-body">
                            <div className="form-line">
                                <div className="form-group">
                                    <label htmlFor="typeObject">Объект</label>
                                    <select className="form-control" id="typeObject">
                                        <option>Option</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-success" onClick={this.save}>Сохранить</button>
                            <button type="button" className="btn btn-default" data-dismiss="modal">Отмена</button>
                        </div>
                    </div>

                </div>
            </div>
        );
    }
}