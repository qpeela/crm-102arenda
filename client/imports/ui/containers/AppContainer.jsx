import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

import history from '/client/imports/startup/client/history';
import './nav.scss';

export default class AppContainer extends Component {
    constructor(props) {
        super(props);

        this.state = this.getMeteorData();
    }

    componentWillMount(){
        if (!this.state.isAuthenticated) {
            history.push('/login');
        }
    }

    componentDidUpdate(){
        if (!this.state.isAuthenticated) {
            history.push('/login');
        }
    }

    getMeteorData(){
        return { isAuthenticated: Meteor.userId() !== null };
    }

    render() {
        return (
            <header>
                <nav className="navbar navbar-default">
                    <div className="container-fluid">
                        <div className="navbar-header">
                            <a className="navbar-brand" href="#">102 Аренда CRM System</a>
                        </div>
                        <ul className="nav navbar-nav">
                            <li>
                                <NavLink exact to="/" activeClassName="active">
                                    Главная
                                </NavLink>
                            </li>
                            <li>
                                <NavLink to="/admin" activeClassName="active">
                                    Админка
                                </NavLink>
                            </li>
                            <li>
                                <NavLink to="/db-objects" activeClassName="active">
                                    Объекты
                                </NavLink>
                            </li>
                            <li>
                                <NavLink to="/clients" activeClassName="active">
                                    Клиенты
                                </NavLink>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
        );
    }
}