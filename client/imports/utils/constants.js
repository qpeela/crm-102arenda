export const typeObject = {
    flat: 'Квартира',
    room: 'Комната',
    house: 'Дом',
    maloSemeika: 'Малосемейка',
    cottedge: 'Коттедж',
};
export const countRooms = {
    '1': '1',
    '2': '2',
    '3': '3',
    '4': '4',
    room: 'Комната',
    '1studio': ' 1-студия',
};
export const district = {
    center: 'Центр',
    prospekt: 'Проспект',
    sip: 'Сипайлово',
    zel: 'Зеленка',
    tcBash: 'ТЦ Башкирия',
    mvd: 'МВД',
    chern: 'Черниковка',
    dem: ' Дема',
    zaton: 'Затон',
    inors: 'Инорс',
    saksha: 'Шакша',
    nizh: 'Нижегородка',
};