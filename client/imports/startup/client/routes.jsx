import React from 'react'
import { Router, Route } from 'react-router-dom';

import history from './history';
// containers
// import MainContainer from '../../ui/containers/MainContainer.jsx'
import AppContainer from '../../ui/containers/AppContainer.jsx';

// pages
// import SignupPage from '../../ui/pages/SignupPage.jsx'
import LoginPage from '../../ui/pages/LoginPage.jsx'
import AdminPanelPage from '../../ui/pages/AdminPanelPage.jsx'
import DbObjectsPage from '../../ui/pages/DbObjectsPage/index';
import DbClientsPage from '../../ui/pages/DbClientsPage/index';
import HomePage from '../../ui/pages/HomePage/index';

export const renderRoutes = () => (
    <Router history={history}>
        <div>
            <AppContainer />
            <div className="container-fluid">
                <Route path="/login" component={LoginPage}/>
                <Route path="/admin" component={AdminPanelPage}/>
                <Route path="/db-objects" component={DbObjectsPage}/>
                <Route path="/clients" component={DbClientsPage}/>
                {/*<Route path="/signup" component={SignupPage}/>*/}
                <Route exact={true} path="/" component={HomePage}/>
            </div>
        </div>
    </Router>
);